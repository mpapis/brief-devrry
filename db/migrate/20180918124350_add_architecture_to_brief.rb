class AddArchitectureToBrief < ActiveRecord::Migration[5.2]
  def change
    add_column :briefs, :architecture, :string
  end
end
