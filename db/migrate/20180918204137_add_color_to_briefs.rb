class AddColorToBriefs < ActiveRecord::Migration[5.2]
  def change
    add_column :briefs, :color, :string
  end
end
