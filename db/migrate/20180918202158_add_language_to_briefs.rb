class AddLanguageToBriefs < ActiveRecord::Migration[5.2]
  def change
    add_column :briefs, :language, :boolean
  end
end
