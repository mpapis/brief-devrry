class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new(role: "user_role") # not logged in
    case user.role
    when "user_role"
      can :create, Brief
    else
      "admin_role"
      can :manage, :all

    end
  end
end